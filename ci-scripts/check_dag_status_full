#!/bin/bash
# Checks a DAG and its jobs are running ok

IGWN_POOL=condor-osg.ligo.caltech.edu
TIMEOUT=60

JOB_BATCH_NAME=${1}

echo "Checking job batch ${JOB_BATCH_NAME}"

# Check DAG is in the queue: only use the lastest
cmd_str=$(echo condor_q -pool ${IGWN_POOL} -constraint \''(JobBatchName=='\"${JOB_BATCH_NAME}\"') && (Cmd=="/usr/bin/condor_dagman") && (Out=="grid-exerciser.dag.lib.out")'\' -af ClusterID)
dagmanjob=$(eval $cmd_str)

echo "ExitCode for condor_q query: $?"

if [[ ! -z ${dagmanjob} ]]; then # If DAG is in the queue, check the status of any child jobs still in the queue

  dagmanout="$(condor_q ${dagmanjob} -af Iwd)/grid-exerciser.dag.dagman.out"

  echo "DAGMan job ID: ${dagmanjob}"
  echo "DAGMan Out: ${dagmanout}"

    # Node problem counter
    problems=0

    # Check wall time for the DAG itself
    dag_entered_current_status=$(condor_q -pool ${IGWN_POOL} ${dagmanjob} -af EnteredCurrentStatus)
    dag_time_in_status=$(($(date +%s) - ${dag_entered_current_status}))
    if [[ ${dag_time_in_status} -gt 9000 ]]; then
      echo "DAG in queue for more than 2.5 hours, noting potential issue"
      problems=$((problems+1))
    else
      echo "DAG in queue for less than 2.5 hours"
    fi

    # Check for htgettoken failure
    echo "Checking htgettoken failures"
    result=$(grep "htgettoken: failure" ${dagmanout} | head -1)
    if [[ ! -z ${result} ]]; then 
      echo "Failure found: ${result}"
      problems=$((problems+1))
    fi

    # Check for RECOVERY mode
    echo "Checking recovery mode"
    result=$(grep "Running in RECOVERY mode" ${dagmanout} | head -1)
    if [[ ! -z ${result} ]]; then 
      echo "Recovery found: ${result}"
      problems=$((problems+1))
    fi

    # Loop through all NODES of the DAG (not the dagman process itself)
    cmd_str=$(echo condor_q -pool ${IGWN_POOL} -constraint \''(GridExerciser==true) && (Cmd=!="/usr/bin/condor_dagman")'\' -af ClusterID)
    for cluster in $(eval ${cmd_str}); do

        # Loop through cluster processes
        for proc in $(condor_q -pool ${IGWN_POOL} -constraint "ClusterID==${cluster}" -af ProcID); do

          unset dagnodename

          until [ ! -z ${dagnodename} ]; do
            read -r dagnodename cmd job_status entered_current_status <<<$(condor_q -pool ${IGWN_POOL} ${cluster}.${proc} -af DAGNodeName Cmd JobStatus EnteredCurrentStatus)
            sleep 1
          done


          echo "---------------------------------------------------"
          echo "Checking job ${cluster}.${proc} in DAG ${dagmanjob}"
          echo "Node: ${dagnodename}"
          echo "Cmd: ${cmd}"

          time_in_status=$(( $(date +%s) - ${entered_current_status}))

          case "$job_status" in

            0) echo "${cluster}.${proc} Unexpanded, U"
              # Probably not good
              ;;
            1) echo "${cluster}.${proc} idle, I"

              echo "Idle, checking if timeout exceeded"
              if [ ${time_in_status} -gt 3600 ]; then
                echo "Idling for > 3600s, test failed"
                #condor_rm ${cluster}.${proc}
                problems=$((problems+1))
              fi
              ;;
            2) echo  "${cluster}.${proc} running, R"
              # Nothing to do
              echo "Running, checking if timeout exceeded"
              if [ ${time_in_status} -gt 1800 ]; then
                site=$(condor_q -pool ${IGWN_POOL} ${cluster}.${proc} -af MATCH_EXP_JOB_GLIDEIN_Site)
                echo "Running for > 1800s, test failed at: ${site}"
                problems=$((problems+1))
                #condor_rm ${cluster}.${proc}
              fi
              ;;
            3) echo  "${cluster}.${proc} removed, X"
              # Not good but we'll deal with this in the post-mortem
              #problems=$((problems+1))
              ;;
            4) echo  "${cluster}.${proc} completed, C"
              # nothing to do
              continue
              ;;
            5) echo  "${cluster}.${proc} held, H"
              # Probably bad, do something
              #condor_rm ${cluster}.${proc}
              problems=$((problems+1))
              ;;
            6) echo "${cluster}.${proc} submission error, E"
              ;;
          esac

        done
      done

      if [ ${problems} -ne 0 ]; then
        echo "FAIL ALERT: Found ${problems} problems"
      else
        echo "SUCCESS"
      fi
      exit ${problems}

    else # grid-exerciser is not in the queue, so query history

      echo "DAG not detected, checking most recent status from history"

      cmd_str="timeout ${TIMEOUT} $(echo condor_history -constraint \''(JobBatchName=='\"${JOB_BATCH_NAME}\"') && (Cmd=="/usr/bin/condor_dagman") && (Out=="grid-exerciser.dag.lib.out")'\' -limit 1 -af DAG_Status)"
      dag_status=$(eval ${cmd_str})

      if [[ -z ${dag_status} ]]; then

        echo "DAG not found in history (within timeout interval ${TIMEOUT}s)"
        exit 124

      else

        echo "Most recent DAG_Status: ${dag_status}"

        if [[ ${dag_status} -eq 4 || ${dag_status} -eq 2 ]]; then

          cmd_str=$(echo condor_history -constraint \''(JobBatchName=='\"${JOB_BATCH_NAME}\"') && (Cmd=="/usr/bin/condor_dagman") && (Out=="grid-exerciser.dag.lib.out")'\' -limit 1 -af Iwd)
          iwd=$(eval ${cmd_str})

          if [[ -f ${iwd}/grid-exerciser.dag.metrics ]]; then

            njobs=$(cat ${iwd}/grid-exerciser.dag.metrics |  python3 -c "import sys, json; print(json.load(sys.stdin)['jobs'])")
            njobs_run=$(cat ${iwd}/grid-exerciser.dag.metrics |  python3 -c "import sys, json; print(json.load(sys.stdin)['total_jobs_run'])")
            jobs_failed=$(cat ${iwd}/grid-exerciser.dag.metrics |  python3 -c "import sys, json; print(json.load(sys.stdin)['jobs_failed'])")
            jobs_succeeded=$(cat ${iwd}/grid-exerciser.dag.metrics |  python3 -c "import sys, json; print(json.load(sys.stdin)['jobs_succeeded'])")
            echo "Jobs run: ${njobs_run}/${njobs}"
            echo "Jobs Failed: ${jobs_failed}/${njobs_run}"
            echo "Jobs Succeeded: ${jobs_succeeded}/${njobs_run}"

            exit 0

          else
            echo "WARNING: ${iwd}/grid-exerciser.dag.metrics not found"
            exit 1
          fi # end test for metrics file

        else

          exit ${dag_status}

        fi # End test of dag_status

      fi # end test for dag in history
fi # End test for dag in queue


