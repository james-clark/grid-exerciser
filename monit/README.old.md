# Elasticsearch

## Users and roles setup

[Upstream docs](https://www.elastic.co/guide/en/cloud-on-k8s/master/k8s-users-and-roles.html)

Get auto-generated secret for default superuser `elastic`:

    kubectl get secret grid-elastic-es-elastic-user -o go-template='{{.data.elastic | base64decode}}'

Check the curl reponse inside the k8s cluster (e.g. through `kubectl exec`) using the service name:

    curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -XGET http://grid-elastic-es-http:9200
    {
      "name" : "grid-elastic-es-elasticsearch-2",
      "cluster_name" : "grid-elastic",
      "cluster_uuid" : "DrdQ6dv9RueGRb4dd9ihGA",
      "version" : {
        "number" : "7.13.0",
        "build_flavor" : "default",
        "build_type" : "docker",
        "build_hash" : "5ca8591c6fcdb1260ce95b08a8e023559635c6f3",
        "build_date" : "2021-05-19T22:22:26.081971330Z",
        "build_snapshot" : false,
        "lucene_version" : "8.8.2",
        "minimum_wire_compatibility_version" : "6.8.0",
        "minimum_index_compatibility_version" : "6.0.0-beta1"
      },
      "tagline" : "You Know, for Search"
    }

Confirm the ingress (mapped to port 9200 already) responds the same way:

    curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -XGET https://grid-elastic.nautilus.optiputer.net
    {
      "name" : "grid-elastic-es-elasticsearch-0",
      "cluster_name" : "grid-elastic",
      "cluster_uuid" : "DrdQ6dv9RueGRb4dd9ihGA",
      "version" : {
        "number" : "7.13.0",
        "build_flavor" : "default",
        "build_type" : "docker",
        "build_hash" : "5ca8591c6fcdb1260ce95b08a8e023559635c6f3",
        "build_date" : "2021-05-19T22:22:26.081971330Z",
        "build_snapshot" : false,
        "lucene_version" : "8.8.2",
        "minimum_wire_compatibility_version" : "6.8.0",
        "minimum_index_compatibility_version" : "6.0.0-beta1"
      },
      "tagline" : "You Know, for Search"
    }


Create user `admin` via API:

    $  curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -H "Content-Type: application/json" -d "@user-admin.json" -X POST https://grid-elastic.nautilus.optiputer.net/_security/user/admin
    {"created":true}%


Where the details of the user are given in the local file: `user-admin.json`:

    {
      "password" : "XXXXXXXXXXXXXXXX",
      "roles" : ["superuser"],
      "full_name" : "Albert Einstein",
      "email" : "albert.einstein@ligo.org"
    }

# Adding data

## Create a test index

    $  curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -X PUT https://grid-elastic.nautilus.optiputer.net/testidx
    {"acknowledged":true,"shards_acknowledged":true,"index":"testidx"}

## Create a test document

Working from the tutorial [here](https://stackify.com/elasticsearch-tutorial/).

    $  curl --user elastic:A6zAwE49G4eJ03o67M36eKsf -X POST https://grid-elastic.nautilus.optiputer.net/my_index/my_type -d '{"user":"James","message":"Hello World!"}' -H "Content-Type: application/json"
    {"_index":"my_index","_type":"my_type","_id":"PuckzXkBOcUl6qIPOl8G","_version":1,"result":"created","_shards":{"total":2,"successful":2,"failed":0},"_seq_no":0,"_primary_term":1}%

Creates index `my_index` and type `my_type`.

Create a document with an ID (e.g. ClusterID):

    $  curl --user elastic:A6zAwE49G4eJ03o67M36eKsf -X POST https://grid-elastic.nautilus.optiputer.net/my_index/my_type/ID123 -d '{"user":"James","message":"Hello World!"}' -H "Content-Type: application/json"
    {"_index":"my_index","_type":"my_type","_id":"ID123","_version":1,"result":"created","_shards":{"total":2,"successful":2,"failed":0},"_seq_no":1,"_primary_term":1}%


## Upload a document

Get a history ad in JSON:

    $ condor_history 38740259.0 -limit 1 -json | ~/bin/jq '.[0]' > ./38740259.0.json

    $  curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -X POST https://grid-elastic.nautilus.optiputer.net/testidx/_doc/1 -d "@example-doc.json" -H "Content-Type: application/json"
    {"_index":"testidx","_type":"_doc","_id":"1","_version":1,"result":"created","_shards":{"total":2,"successful":2,"failed":0},"_seq_no":0,"_primary_term":1}%

NOTES: 

1. Currently (?) unable to see the es server from CIT
1. The document does not have a time index.  We should fix that.

### Adding a timestamp

Here's an example rucio document:

		Object
		_id:"jokqzXkBrhYd32FqRToq"
		_type:"doc"
		_index:"rucio-events-2021.06.02"
		sort:Array[1622644769954,6799]
		highlight:undefined
		type:"rucio-event"
		created_at:"2021-06-02 14:39:22.851258"
		event_type:"deletion-done"
		@timestamp:"2021-06-02T14:39:29.954Z"
		payload:Object
		bytes:1000000
		duration:0.11117291450500488
		name:"automatix-test-GWF-26a477281bef473abd8273ed5697cd56"
		url:"srm://storm-fe-archive.cr.cnaf.infn.it:8444/srm/managerv2?SFN=/virgoplain/rucio/TEST/automatix-test-GWF-26a477281bef473abd8273ed5697cd56"
		rse:"CNAF"
		file-size:1000000
		scope:"TEST"
		protocol:"srm"
		@version:"1"

Get that `@timestamp` value with 
		$  date -u +%Y-%m-%dT%R.%3NZ
		2021-06-02T15:00.476Z

Added that as a `@timestamp` field in the doc itself (may be a better way to do this).

Created a new doc with date-stamped index:

		$  curl --user elastic:A6zAwE49G4eJ03o67M36eKsf -X POST https://grid-elastic.nautilus.optiputer.net/testidx-2021-06-02/_doc -d "@example-date-doc.json" -H "Content-Type: application/json"
		{"_index":"testidx-2021-06-02","_type":"_doc","_id":"P-dOzXkBOcUl6qIPD18I","_version":1,"result":"created","_shards":{"total":2,"successful":1,"failed":0},"_seq_no":0,"_primary_term":1}%                                                  

Hmm, still getting "The indices which match this index pattern don't contain
any time fields." from kibana - don't think the `@timestamp` is supposed to be
in the document itself.

Trying an "ingest node pipeline" (via kibana).

Delete old docs:

		$  curl --user elastic:A6zAwE49G4eJ03o67M36eKsf -X DELETE https://grid-elastic.nautilus.optiputer.net/testidx-2021-06-02/_doc/P-dOzXkBOcUl6qIPD18I
		{"_index":"testidx-2021-06-02","_type":"_doc","_id":"P-dOzXkBOcUl6qIPD18I","_version":2,"result":"deleted","_shards":{"total":2,"successful":2,"failed":0},"_seq_no":2,"_primary_term":1}%  

Re-upload using the ingest pipeline (created via the Kibana gui - converts the `CompletionDate` field to a timestamp):

		$  curl --user elastic:A6zAwE49G4eJ03o67M36eKsf -X POST https://grid-elastic.nautilus.optiputer.net/testidx-2021-06-02/_doc\?pipeline\=test-date -d "@example-doc.json" -H "Content-Type: application/json"
		{"_index":"testidx-2021-06-02","_type":"_doc","_id":"nEHSzXkB9lJaYKNX6UZM","_version":1,"result":"created","_shards":{"total":2,"successful":2,"failed":0},"_seq_no":5,"_primary_term":1}%

Now at we have a document.  Note that it has ["Multi Fields"](https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html), which seems to be expected.

		It is often useful to index the same field in different ways for different
		purposes. This is the purpose of multi-fields. For instance, a string field
		could be mapped as a text field for full-text search, and as a keyword field
		for sorting or aggregations:

What happens if we post the actual JSON from condor (without accessing the zeroth field?) -- it fails

But here's a full command which pipes the `condor_history` in directly:

		$ curl --user elastic:A6zAwE49G4eJ03o67M36eKsf -X POST https://grid-elastic.nautilus.optiputer.net/testidx/_doc\?pipeline\=test-date -d "$(condor_history 38792853.6 -limit 1 -json | ~/bin/jq '.[0]')" -H "Content-Type: application/json"
		{"_index":"testidx","_type":"_doc","_id":"nUHszXkB9lJaYKNXpkav","_version":1,"result":"created","_shards":{"total":2,"successful":2,"failed":0},"_seq_no":2,"_primary_term":1}

### Designing the document

Types of event we're going to be interested in:

* DAG complete
* Job complete
* DAG submitted (potential)
* Job submitted (potential)

Suggest indices: `grid-exerciser-$(date)` and types included in the JSON similar to rucio:
event:

    "type": "grid-exerciser-event",
    "created_at": "2021-06-02 14:39:20.517554",
    "event_type": "job-complete",

Probably easiest just to append these fields in the post-processing script e.g.:

		$ condor_history 38792853.7 -limit 1 -json | ~/bin/jq ".[0] += {\"type\":\"grid-exerciser-event\", \"event_type\":\"job-complete\", \"created_at\": \"$(date -u +'%Y-%m-%dT%H:%M:%SXX')\"}" | ~/bin/jq '.[0]'

And use grid-exerciser index with an ingest pipeline which appends the week to the index

The JSON for the ingestion pipeline is:

<<<<<<< HEAD
## Elasticsearch roles

In Kibana: Stack Management >> Roles >> create role: `grid-exerciser` with:

* Cluster privileges: `monitor`
* Index privileges: `testidx`:`all` (to be updated with `grid-exerciser-*` index)

or equivalently through API with JSON:

    {
      "cluster": [
        "monitor"
      ],
      "indices": [
        {
          "names": [
            "rucio-event*"
          ],
          "privileges": [
            "all"
            ],
            "allow_restricted_indices": false
        }
      ],
      "applications": [],
      "run_as": [],
      "metadata": {},
      "transient_metadata": {
        "enabled": true
      }
    }

Create a user `grid-exerciser` with role `grid-exerciser`

    $  curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -H "Content-Type: application/json" -d "@user-grid-exerciser.json" -X POST https://grid-elastic.nautilus.optiputer.net/_security/user/grid-exerciser
    {"created":true}%

where:

    {
      "password" : "XXXXXXXXXXXXXXXX",
      "roles" : ["grid-exerciser"],
      "full_name" : "Grid Exerciser",
      "email" : "james.clark@ligo.org"
    }


# Unexpected problem: occassionally boolean fields

An existing document was posted to elasticsearch which had `PeriodicRelease=False`.
A subsequent document has `/Expr((HoldReasonCode == 45) && (HoldReasonSubCode == 0))/`.
Elasticsearch is now freaking out because the `PeriodicRelease` field should be a boolean.

I would rather avoid pruning the classad so how can I force this to be a string?

Could do this with an ingest node pipeline but that still forces us to enumerate specific fields.

This is will affect fields which have values `false` or `Expr`.

Options:

1. Ingest pipeline to modify attribute value type - requires 1 processor per
   attribute: explicit enumeration of attributes.
1. Delete all lines from classad with `false` value prior to ingestion - loss of information.
1. Pre-process in logstash and put the value in different fields depending on the type - needs logstash
1. Convert: `true->1`, `false->0` prior to ingestion.

# Kibana index pattern

Create a **kibana** index pattern, taking care to specify the time field!
