#!/bin/bash -e

host="https://condor-elastic.nrp-nautilus.io"
index="htcondor-igwn*"
user="htcondor-read"
pass="Qm#%-y5?W_&6"

curl -XGET --user ${user}:${pass} --header 'Content-Type: application/json' "${host}/${index}/_search?pretty=true" -d \
  '{
    "query": {
      "match": {
        "match_glidein_site": "SUT-OzStar"
        }
    }
}'

#     '{
#     "query": {
#       "match": {
#         "match_glidein_site.keyword": {
#           "query": "LIGO-CIT"
#         }
#       }
#     }
#   }'
