#!/bin/bash -e

if [ -z ${SCHEDDS} ] || [ ${SCHEDDS} == "pilots" ]; then
  echo "Will report pilot job histories"
  echo "Finding CE schedds"
  SCHEDDS=$(condor_status -any -pool gfactory-2.opensciencegrid.org -const '(MyType=?="glidefactory") && (stringListMember("LIGO",GLIDEIN_Supported_VOs) || stringListMember("LIGOGPU",GLIDEIN_Supported_VOs) || stringListMember("VIRGO",GLIDEIN_Supported_VOs) || stringListMember("VIRGOGPU",GLIDEIN_Supported_VOs))'   -af GLIDEIN_Gatekeeper | sort | awk '{print $1}' | paste -s -d, -)
  echo "Found: ${SCHEDDS}"
fi

if [ ${MODE} == "history" ]; then

  echo "Running adstash for job histories"
  eval /usr/bin/python3 -u /usr/bin/condor_adstash \
    --log_file ${LOG_FILE} \
    --se_host ${ES_HOST} \
    --se_username ${ES_USERNAME} \
    --se_index_name ${ES_INDEX_NAME} \
    --collectors ${COLLECTORS} \
    --schedds ${SCHEDDS} \
    --log_level INFO \
    --checkpoint_file ${CHECKPOINT_FILE} \
    --sample_interval ${SAMPLE_INTERVAL} \
    --schedd_history \
    --schedd_history_max_ads ${SCHEDD_HISTORY_MAX_ADS} \
    --se_bunch_size ${NUM_DOCS} \
    --standalone
    #--se_use_https \
     
    echo "Adstash for job histories complete"

elif [ ${MODE} == "epoch_history" ]; then

  echo "Running adstash for job epoch histories"
  eval /usr/bin/python3 -u /usr/bin/condor_adstash \
    --log_file ${LOG_FILE} \
    --se_host ${ES_HOST} \
    --se_username ${ES_USERNAME} \
    --se_index_name ${ES_INDEX_NAME} \
    --collectors ${COLLECTORS} \
    --schedds ${SCHEDDS} \
    --log_level INFO \
    --checkpoint_file ${CHECKPOINT_FILE} \
    --sample_interval ${SAMPLE_INTERVAL} \
    --schedd_job_epoch_history \
    --schedd_history_max_ads ${SCHEDD_HISTORY_MAX_ADS} \
    --se_bunch_size ${NUM_DOCS} \
    --standalone
    #--se_use_https \
    
    echo "Adstash for job epoch histories complete"

elif [ ${MODE} == "transfer_epoch_history" ]; then

  echo "Running adstash for transfer epoch histories"
  eval /usr/bin/python3 -u /usr/bin/condor_adstash \
    --log_file ${LOG_FILE} \
    --se_host ${ES_HOST} \
    --se_username ${ES_USERNAME} \
    --se_index_name ${ES_INDEX_NAME} \
    --collectors ${COLLECTORS} \
    --schedds ${SCHEDDS} \
    --log_level INFO \
    --checkpoint_file ${CHECKPOINT_FILE} \
    --sample_interval ${SAMPLE_INTERVAL} \
    --schedd_transfer_epoch_history \
    --schedd_history_max_ads ${SCHEDD_HISTORY_MAX_ADS} \
    --se_bunch_size ${NUM_DOCS} \
    --standalone
    #--se_use_https \
    
    echo "Adstash for job transfer epoch histories complete"

fi

mv ${LOG_FILE} ${LOG_FILE}.bk
