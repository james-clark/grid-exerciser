# Monitoring configuration for HTCondor \& `condor_adstash`

## Elasticsearch deployment

Deploy the elasticsearch cluster, kibana and their ingresses with the kustomization file:

    kubectl apply -k .

Retrieve auto-generated secret for default superuser `elastic`:

    kubectl get secret grid-elastic-es-elastic-user -o go-template='{{.data.elastic | base64decode}}'

Check the curl reponse inside the k8s cluster (e.g. through `kubectl exec`) using the service name:

(NOTE: some URLs / details may differ)

    curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -XGET http://grid-elastic-es-http:9200
    {
      "name" : "grid-elastic-es-elasticsearch-2",
      "cluster_name" : "grid-elastic",
      "cluster_uuid" : "DrdQ6dv9RueGRb4dd9ihGA",
      "version" : {
        "number" : "7.13.0",
        "build_flavor" : "default",
        "build_type" : "docker",
        "build_hash" : "5ca8591c6fcdb1260ce95b08a8e023559635c6f3",
        "build_date" : "2021-05-19T22:22:26.081971330Z",
        "build_snapshot" : false,
        "lucene_version" : "8.8.2",
        "minimum_wire_compatibility_version" : "6.8.0",
        "minimum_index_compatibility_version" : "6.0.0-beta1"
      },
      "tagline" : "You Know, for Search"
    }

Confirm the ingress (mapped to port 9200 already) responds the same way:

    curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -XGET https://grid-elastic.nautilus.optiputer.net
    {
      "name" : "grid-elastic-es-elasticsearch-0",
      "cluster_name" : "grid-elastic",
      "cluster_uuid" : "DrdQ6dv9RueGRb4dd9ihGA",
      "version" : {
        "number" : "7.13.0",
        "build_flavor" : "default",
        "build_type" : "docker",
        "build_hash" : "5ca8591c6fcdb1260ce95b08a8e023559635c6f3",
        "build_date" : "2021-05-19T22:22:26.081971330Z",
        "build_snapshot" : false,
        "lucene_version" : "8.8.2",
        "minimum_wire_compatibility_version" : "6.8.0",
        "minimum_index_compatibility_version" : "6.0.0-beta1"
      },
      "tagline" : "You Know, for Search"
    }

### Elasticsearch user configuration

Create user `admin` via API:

    $  curl --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX -H "Content-Type: application/json" -d "@user-admin.json" -X POST https://grid-elastic.nautilus.optiputer.net/_security/user/admin
    {"created":true}%


Where the details of the user are given in the local file: `user-admin.json`:

    {
      "password" : "XXXXXXXXXXXXXXXX",
      "roles" : ["superuser"],
      "full_name" : "Albert Einstein",
      "email" : "albert.einstein@ligo.org"
    }

**Remaining configuration performed through Kibana UI**:

Now log into kibana as `admin` or `elastic` and create role `htcondor` with
cluster privileges set to monitor and all privileges on all indices (because we
want the role to be able to create indices):

    Stack management -> Roles -> Create Role

1. Cluster privileges: monitor
1. Indices: * /
1. Index privileges: all

Then create a user `htcondor` with that role:

    Stack management -> Roles -> Create User

### Ingest node pipeline

We can enhance the data from adstash in a number of ways:

1. Add a time-stamp to the elasticsearch index for more efficient index management
1. Add a `@timestamp` field to the documents using (e.g.) job `CompletionDate`
1. Add a `cmd_name` field to docs to report the *basename* of the application
   being executed.  This makes it easier to use template variables in Grafana
   to identify pipelines / processes.

NOTE: I have used `CompletionDate` directly before but `@timestamp` is the
default expected in Grafana, where it is handled more gracefully (e.g. in the
presentation of the `logs` metric)

To create the ingest node pipeline in Kibana:

    Ingest Node Pipelines -> Create a pipeline

And add processors:

* `Date index name` with `Index name prefix: htcondor-igwn-`, `Field: CompletionDate`, `Rounding: Week` and `Date Formats: UNIX`
* `Date`: `Field: CompletionDate` and `Date Formats: UNIX`
* `Grok`: `Field: Cmd` and 2 patterns: `^.*/%{DATA:cmd_name}$` and `^%{DATA:cmd_name}$` (see [grok-patterns](https://github.com/elastic/elasticsearch/blob/master/libs/grok/src/main/resources/patterns/ecs-v1/grok-patterns))

Check the PUT request (save and `show request`):

    PUT _ingest/pipeline/htcondor-igwn
    {
      "description": "Enrich condor history classads",
      "processors": [
        {
          "date_index_name": {
            "field": "CompletionDate",
            "date_rounding": "w",
            "index_name_prefix": "htcondor-igwn-",
            "date_formats": [
              "UNIX"
            ]
          }
        },
        {
          "date": {
            "field": "CompletionDate",
            "formats": [
              "UNIX"
            ]
          }
        },
        {
          "grok": {
            "field": "Cmd",
            "patterns": [
              "^.*/%{DATA:cmd_name}$",
              "^%{DATA:cmd_name}$"
            ]
          }
        }
      ]
    }

Save the pipeline as `htcondor-igwn`.

Create an empty index `htcondor-igwn`, we will configure the ingest pipeline to redirect documents to the time-stamped version of this:

    curl -XPUT -v --user elastic:XXXXXXXXXXXXXXXXXXXXXXXX https://condor-elastic.nautilus.optiputer.net/htcondor-igwn

Finally, configure that index to use this ingest node pipeline:

    Stack management -> Index management -> Edit settings (for the htcondor-igwn index)

And add the line:

    "index.default_pipeline": "htcondor-igwn"

Now when we run `adstash`, documents will be indexed into e.g. `htcondor-igwn-2021-10-04`.

## `condor_adstash` configuration

### Standalone

Any user with access to elasticsearch can run `condor_adstash`:

    $ _CONDOR_ADSTASH_ES_PASSWORD_FILE=/path/to/file  condor_adstash \
                                       --log_file adstash.log \
                                       --es_host condor-elastic.nautilus.optiputer.net:443 \
                                       --es_username htcondor --es_use_https \
                                       --es_index_name htcondor-igwn \
                                       --collectors condor-osg.ligo.caltech.edu \
                                       --schedds ldas-osg.ligo.caltech.edu \
                                       --log_level DEBUG \
                                       --checkpoint_file adstash_checkpoint.json \
                                       --sample_interval 100 \
                                       --schedd_history \
                                       --standalone

where `_CONDOR_ADSTASH_ES_PASSWORD_FILE=/path/to/file` is a plain text file
with the basic auth login for user `htcondor` in elasticsearch.  

Security note: set `chmod 0400 /path/to/file`

Note this command will query only `ldas-osg` for `schedd` histories; `startd` histories are also available.

### Managed by `condor_master`

To run the daemonised `condor_adstash`, make a copy of the example adstash config, e.g:

    cp /usr/share/doc/condor-X.X.X/examples/condor_config.local.adstash /etc/condor/config.d/99-adstash

And:

1. Add: `ADSTASH = $(BIN)/condor_adstash` (maybe there's a more appropriate place to define this?)
1. Set: `ADSTASH_ES_PASSWORD_FILE =
   /var/lib/condor/adstash_credentials/.es_pass`, where `.es_pass` is the
   root-owned file with the `htcondor` user password for elasticsearch and 0400
   permissions.
1. Update remaining options according to your needs (see standalone example above). 

Finally, reconfigure the local master:

    $ condor_reconfig

And that's it!

## Kibana configuration

Create an index pattern:

    Kibana -> Index Patterns -> "htcondor-igwn-*"

Set:

    Time field: @timestamp

Now go to "Discover" in Kibana and you should see results.

