#!/usr/bin/env python3

import datetime
import json
import csv
import argparse
## import dask libraries
import dask.dataframe as dd
import pandas as pd

#importing packages for sending email
import os
import matplotlib.pyplot as plt

#asyncio request
import asyncio
from elasticsearch import Elasticsearch, AsyncElasticsearch
from elasticsearch.helpers import async_scan



class OptionParser:
    def __init__(self):
        """User based option parser"""
        self.parser = argparse.ArgumentParser(prog="PROG")
        self.parser.add_argument(
            "--host",
            action="store",
            dest="host",
            default="",
            help="elastic search host",
            required=True,
        )
        self.parser.add_argument(
            "--port",
            action="store",
            dest="port",
            default="",
            help="listening port of elasticsearch host for quering",
            required=True,
        )
        self.parser.add_argument(
            "--username",
            action="store",
            dest="username",
            default="",
            help="username for elasticsearch query",
            required=True,

        )
        self.parser.add_argument(
            "--password",
            action="store",
            dest="password",
            default="",
            help="password for elasticsearch query",
            required=True,
        )
        self.parser.add_argument(
            "--days",
            action="store",
            dest="days",
            default="1",
            help="no. of days of data you want to analyse from elasticsearch ",
            required=True,
        )
        self.parser.add_argument(
            "--index",
            action="store",
            dest="index",
            default=False,
            help="input the index name of elasticsearch",
            required=True,
        )
        


def timeassign(attributes, days):
    # setting end_time to today's midnight time in UTC
    end_time = datetime.datetime.now(datetime.timezone.utc)
    # setting start_time to end_time - no. of days
    start_time = end_time - datetime.timedelta(days=days)
    start_time = start_time.replace(tzinfo=datetime.timezone.utc)
    
    time_filter = {
        "end_time" : int(end_time.strftime("%s")),
        "start_time" : int(start_time.strftime("%s")),
        "end_time_human_readable" : start_time.strftime("%Y-%m-%d %H:%M:%S %Z"),
        "start_time_human_readable" : end_time.strftime("%Y-%m-%d %H:%M:%S %Z")
    }
    dask_time_filter = {
        "T_24hrs" : (int((end_time - datetime.timedelta(days=1)).strftime("%s")), int(end_time.strftime("%s"))),
        "T_7days" : (int((end_time - datetime.timedelta(days=7)).strftime("%s")), int(end_time.strftime("%s"))),
        "T_30days" : (int((end_time - datetime.timedelta(days=30)).strftime("%s")), int(end_time.strftime("%s")))}

    grafana_timestamp = {k: tuple(map(lambda x: x*1000, v)) for k, v in dask_time_filter.items()}
    grafana_dashboard_link_cittest = {k: f'https://grafana-nautilus.ligo.org/d/AkTReP4nk/igwn-grid-analytics?orgId=1&from={v[0]}&to={v[1]}&var-timezone=UTC&var-cmd=All&var-binning=5m&var-bargroup=ligosearchtag&var-ligosearchuser=All&var-ligosearchtag=cit.oneshot.test&var-ligosearchtag=cit.scheduled.test&var-sites=All' for k, v in grafana_timestamp.items()}

    grafana_dashboard_link_alljobs = {k: f'https://grafana-nautilus.ligo.org/d/AkTReP4nk/igwn-grid-analytics?orgId=1&from={v[0]}&to={v[1]}&var-timezone=UTC&var-cmd=All&var-binning=5m&var-bargroup=ligosearchtag&var-ligosearchuser=All&var-ligosearchtag!=cit.oneshot.test&var-ligosearchtag!=cit.scheduled.test&var-sites=Alll' for k, v in grafana_timestamp.items()}

    query = {
        "_source" : attributes,
        "query": {
            "range": {
                "RecordTime": {
                    "gte": time_filter["start_time"],
                    "lte": time_filter["end_time"]
                    }
                }
            }
        }
    return query , dask_time_filter , grafana_dashboard_link_cittest, grafana_dashboard_link_alljobs , time_filter

async def writer(esclient, hosts, query, index, username, password , datafile, attributes):
    # takes elastic generator function and writes json object to csv file
    with open(datafile, 'w') as csvfile:  # Just use 'w' mode in 3.x
        writer = csv.DictWriter(csvfile, fieldnames=attributes)
        writer.writeheader()
        try:
            async for doc in async_scan(
                client=esclient,
                query=query,
                index=index,
                scroll='5m',
                size=10000,
                preserve_order=True
            ):
                writer.writerow(doc['_source'])
        except Exception as e:
            print(f"An error occurred during async_scan: {str(e)}")

def JobdataAnalyser(data_filename, dask_time_filter, attributes):
    '''
    takes the csv file containing job information and returns grid site Job count & success rate dataframe
    '''
    try :
        with open(data_filename, 'r', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            fileheaders = list(reader.fieldnames)
        if fileheaders == attributes:
            pass
    except:
        print("attribute is missing in the filename")
    
    df = (dd.read_csv(
        data_filename,
        assume_missing=True, 
        blocksize='64MB', 
        low_memory=False,
        dtype={
            'ExitCode': 'Float64',
            'RecordTime' : 'int64',
            }
        )
        ).rename(columns = lambda x:x.replace('@',''))

    # drop duplicate rows and reset index
    df = df.drop_duplicates().reset_index()

    # success percentage calculation for jobs at each site
    day_data = df['RecordTime'].apply(lambda x: 1 if (x > dask_time_filter['T_24hrs'][0]) & (x < dask_time_filter['T_24hrs'][1]) else 0 , meta=pd.Series(dtype="bool"))
    week_data = df['RecordTime'].apply(lambda x: 1 if (x > dask_time_filter['T_7days'][0]) & (x < dask_time_filter['T_7days'][1]) else 0 , meta=pd.Series(dtype="bool"))
    month_data = df['RecordTime'].apply(lambda x: 1 if (x > dask_time_filter['T_30days'][0]) & (x < dask_time_filter['T_30days'][1]) else 0 , meta=pd.Series(dtype="bool"))
    # Define the conditions and the function to apply
    def apply_condition_day(df, column_name):
        if (df['RecordTime'] > dask_time_filter['T_24hrs'][0]) & (df['RecordTime'] < dask_time_filter['T_24hrs'][1]):
            return df[column_name]
        else:
            return 0
    def apply_condition_week(df, column_name):
        if (df['RecordTime'] > dask_time_filter['T_7days'][0]) & (df['RecordTime'] < dask_time_filter['T_7days'][1]):
            return df[column_name]
        else:
            return 0
    def apply_condition_month(df, column_name):
        if (df['RecordTime'] > dask_time_filter['T_30days'][0]) & (df['RecordTime'] < dask_time_filter['T_30days'][1]):
            return df[column_name]
        else:
            return 0
    
    df['CommittedTime_day_data'] = df.apply(apply_condition_day, args=("CommittedTime",), axis=1, meta=('CommittedTime_day_data', 'float64'))
    df['CommittedTime_week_data'] = df.apply(apply_condition_week,args=("CommittedTime",), axis=1, meta=('CommittedTime_week_data', 'float64'))
    df['CommittedTime_month_data'] = df.apply(apply_condition_month,args=("CommittedTime",), axis=1, meta=('CommittedTime_month_data', 'float64'))

    df['RemoteWallClockTime_day_data'] = df.apply(apply_condition_day,args=("RemoteWallClockTime",), axis=1, meta=('RemoteWallClockTime_day_data', 'float64'))
    df['RemoteWallClockTime_week_data'] = df.apply(apply_condition_week,args=("RemoteWallClockTime",), axis=1, meta=('RemoteWallClockTime_week_data', 'float64'))
    df['RemoteWallClockTime_month_data'] = df.apply(apply_condition_month,args=("RemoteWallClockTime",), axis=1, meta=('RemoteWallClockTime_month_data', 'float64'))

    ########### Job count & success rate Report for ligosearchtag == "cit.scheduled.test" starts here  ##########
    # filter for ligosearchtag , and aggregate based on time intervals
    ddf = (df
        .loc[df['ligosearchtag'].isin(['cit.scheduled.test', 'cit.oneshot.test'])]
        .assign(T_24hrs=day_data)
        .assign(T_7days=week_data)
        .assign(T_30days=month_data)
        .groupby(['match_exp_job_site', 'ExitCode'])
        #.agg({"T_24hrs": "sum", "T_7days": "sum" , "T_30days": "sum", "CommittedTime" : "sum" , "RemoteWallClockTime" : "sum"})
        .agg({"T_24hrs": "sum", "T_7days": "sum" , "T_30days": "sum" ,
          "CommittedTime_day_data" : "sum" , "CommittedTime_week_data" : "sum", "CommittedTime_month_data" : "sum",
          "RemoteWallClockTime_day_data" : "sum" , "RemoteWallClockTime_week_data" : "sum", "RemoteWallClockTime_month_data" : "sum",
         })
        .reset_index()  
    )


    # calculate Job count & success rate for each time period ( day, week, month)
    result = (ddf
            .assign(count_T_24hrs = ddf.groupby('match_exp_job_site')['T_24hrs'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(count_T_7days = ddf.groupby('match_exp_job_site')['T_7days'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(count_T_30days = ddf.groupby('match_exp_job_site')['T_30days'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(T_24hrs_success_rate = 100 * ddf['T_24hrs'] / ddf.groupby('match_exp_job_site')['T_24hrs'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(T_7days_success_rate = 100 * ddf['T_7days'] / ddf.groupby('match_exp_job_site')['T_7days'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(T_30days_success_rate = 100 * ddf['T_30days'] / ddf.groupby('match_exp_job_site')['T_30days'].transform('sum', meta=pd.Series(dtype="float64")))
            #.assign(T_24hrs_goodput = 100 * ddf.groupby('match_exp_job_site')['CommittedTime'].transform('sum', meta=pd.Series(dtype="float64")) / ddf.groupby('match_exp_job_site')['RemoteWallClockTime'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(goodput_24hrs = 100 * ddf.groupby('match_exp_job_site')['CommittedTime_day_data'].transform('sum', meta=pd.Series(dtype="float64")) / ddf.groupby('match_exp_job_site')['RemoteWallClockTime_day_data'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(goodput_week = 100 * ddf.groupby('match_exp_job_site')['CommittedTime_week_data'].transform('sum', meta=pd.Series(dtype="float64")) / ddf.groupby('match_exp_job_site')['RemoteWallClockTime_week_data'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(goodput_month = 100 * ddf.groupby('match_exp_job_site')['CommittedTime_month_data'].transform('sum', meta=pd.Series(dtype="float64")) / ddf.groupby('match_exp_job_site')['RemoteWallClockTime_month_data'].transform('sum', meta=pd.Series(dtype="float64")))
            .round(decimals=0)
            .loc[(ddf.ExitCode == 0)] 
            .reset_index(drop=True)   
            .drop(columns=['ExitCode','T_24hrs','T_7days' ,'T_30days'])
        )
    
    #computing result using .compute method and converting to pandas dataframe
    result_df = result.compute()
    result_df = (result_df
            .sort_values('T_24hrs_success_rate', ascending=False)
            .replace("Unknown", "local_pool_LIGO-CIT")
            .reindex(columns=['match_exp_job_site','count_T_24hrs', 'T_24hrs_success_rate','goodput_24hrs','count_T_7days', 'T_7days_success_rate' , 'goodput_week', 'count_T_30days' , 'T_30days_success_rate' , 'goodput_month'])
    )          
    ########### Job count & success rate Report for ligosearchtag == "cit.scheduled.test" ends here  ##########

    ########### Job count & success rate Report for all tags starts here  ##########
    # filter for ligosearchtag , and aggregate based on time intervals

    ddf_all = (df
        .loc[~df['ligosearchtag'].isin(['cit.scheduled.test', 'cit.oneshot.test'])]
        .assign(T_24hrs=day_data)
        .assign(T_7days=week_data)
        .assign(T_30days=month_data)
        .groupby(['match_exp_job_site', 'ExitCode'])
        # .agg({"T_24hrs": "sum", "T_7days": "sum" , "T_30days": "sum"})
        .agg({"T_24hrs": "sum", "T_7days": "sum" , "T_30days": "sum" ,
          "CommittedTime_day_data" : "sum" , "CommittedTime_week_data" : "sum", "CommittedTime_month_data" : "sum",
          "RemoteWallClockTime_day_data" : "sum" , "RemoteWallClockTime_week_data" : "sum", "RemoteWallClockTime_month_data" : "sum",
         })
        .reset_index() 
    )


    # calculate Job count & success rate for each time period ( day, week, month)
    result_all = (ddf_all
            .assign(count_T_24hrs = ddf_all.groupby('match_exp_job_site')['T_24hrs'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(count_T_7days = ddf_all.groupby('match_exp_job_site')['T_7days'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(count_T_30days = ddf_all.groupby('match_exp_job_site')['T_30days'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(T_24hrs_success_rate = 100 * ddf_all['T_24hrs'] / ddf_all.groupby('match_exp_job_site')['T_24hrs'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(T_7days_success_rate = 100 * ddf_all['T_7days'] / ddf_all.groupby('match_exp_job_site')['T_7days'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(T_30days_success_rate = 100 * ddf_all['T_30days'] / ddf_all.groupby('match_exp_job_site')['T_30days'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(goodput_24hrs = 100 * ddf_all.groupby('match_exp_job_site')['CommittedTime_day_data'].transform('sum', meta=pd.Series(dtype="float64")) / ddf_all.groupby('match_exp_job_site')['RemoteWallClockTime_day_data'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(goodput_week = 100 * ddf_all.groupby('match_exp_job_site')['CommittedTime_week_data'].transform('sum', meta=pd.Series(dtype="float64")) / ddf_all.groupby('match_exp_job_site')['RemoteWallClockTime_week_data'].transform('sum', meta=pd.Series(dtype="float64")))
            .assign(goodput_month = 100 * ddf_all.groupby('match_exp_job_site')['CommittedTime_month_data'].transform('sum', meta=pd.Series(dtype="float64")) / ddf_all.groupby('match_exp_job_site')['RemoteWallClockTime_month_data'].transform('sum', meta=pd.Series(dtype="float64")))
            .round(decimals=0)
            .loc[(ddf_all.ExitCode == 0)] 
            .reset_index(drop=True)   
            .drop(columns=['ExitCode','T_24hrs','T_7days' ,'T_30days'])
        )
    
    #computing result using .compute method and converting to pandas dataframe
    result_all_df = result_all.compute()
    result_all_df = (result_all_df
            .sort_values('T_24hrs_success_rate', ascending=False)
            .replace("Unknown", "local_pool_LIGO-CIT")
            .reindex(columns=['match_exp_job_site','count_T_24hrs', 'T_24hrs_success_rate','goodput_24hrs','count_T_7days', 'T_7days_success_rate' , 'goodput_week', 'count_T_30days' , 'T_30days_success_rate' , 'goodput_month'])
    )          
    ########### Job count & success rate Report for all tags ends here  ##########

    ########### Tag Efficiency Report code Starts here  ##########

    # calculating ligosearchtag efficiency report
    tagdf = (df
        .assign(T_24hrs=day_data)
        .assign(T_7days=week_data)
        .assign(T_30days=month_data)
        .groupby(['ligosearchtag', 'ExitCode'])
        #.agg({"T_24hrs": "sum", "T_7days": "sum" , "T_30days": "sum"})
        .agg({"T_24hrs": "sum", "T_7days": "sum" , "T_30days": "sum" 
        #   "CommittedTime_day_data" : "sum" , "CommittedTime_week_data" : "sum", "CommittedTime_month_data" : "sum",
        #   "RemoteWallClockTime_day_data" : "sum" , "RemoteWallClockTime_week_data" : "sum", "RemoteWallClockTime_month_data" : "sum",
         })
        .reset_index()
    )

    # calculate Job count & success rate for each time period ( day, week, month)
    tagresult = (tagdf
        .assign(count_T_24hrs = tagdf.groupby('ligosearchtag')['T_24hrs'].transform('sum', meta=pd.Series(dtype="float64")))
        .assign(count_T_7days = tagdf.groupby('ligosearchtag')['T_7days'].transform('sum', meta=pd.Series(dtype="float64")))
        .assign(count_T_30days = tagdf.groupby('ligosearchtag')['T_30days'].transform('sum', meta=pd.Series(dtype="float64")))
        .assign(T_24hrs_success_rate = 100 * tagdf['T_24hrs'] / tagdf.groupby('ligosearchtag')['T_24hrs'].transform('sum', meta=pd.Series(dtype="float64")))
        .assign(T_7days_success_rate = 100 * tagdf['T_7days'] / tagdf.groupby('ligosearchtag')['T_7days'].transform('sum', meta=pd.Series(dtype="float64")))
        .assign(T_30days_success_rate = 100 * tagdf['T_30days'] / tagdf.groupby('ligosearchtag')['T_30days'].transform('sum', meta=pd.Series(dtype="float64")))
        .round(decimals=0)
        .loc[(tagdf.ExitCode == 0)]
        .reset_index(drop=True)   
        .drop(columns=['ExitCode','T_24hrs','T_7days' ,'T_30days'])
    )
    # computing ligosearchtag efficienccy dataframe
    tagresult_df = tagresult.compute()
    #rearranging columns in dataframe
    tagresult_df = (tagresult_df
                    .sort_values('T_24hrs_success_rate', ascending=False)
                    .reindex(columns=['ligosearchtag','count_T_24hrs', 'T_24hrs_success_rate','count_T_7days', 'T_7days_success_rate', 'count_T_30days' , 'T_30days_success_rate' ])
    )
    
    ########### Tag Efficiency Report code ends here  ##########


    return result_df , result_all_df, tagresult_df

def make_pretty(styler):
    styler.set_table_styles([
    {'selector': 'th.col_level_2', 
     'props': [('border', '1px solid #d9d9d9'), ('border-left', '1px solid black'), ('border-right', '1px solid black')]},
    {'selector': 'th.col_level_1',
     'props': [('background-color', 'lightgray'),
                 ('color', 'black'),
                 ('font-weight', 'bold'),
                 ('border-bottom', '2px solid black'),
                 ('border-top', '2px solid black'),
                 ('border-left', '2px solid black'),
                 ('border-right', '2px solid black')]},
    {'selector': 'th', 
     'props': [('background-color', '#b8daff'),
              ('color', '#333'),
              ('font-family', 'Arial'),
              ('font-size', '12px'),
              ('font-weight', 'bold'),
              ('text-align', 'center')]},
    {'selector': 'td', 
     'props': [('border', '1px solid #d9d9d9'),
              ('font-family', 'Arial'),
              ('font-size', '12px'),
              ('text-align', 'center')]},
    {'selector': 'tr:hover td', 
     'props': [('background-color', '#f9f9f9')]}
    
    ])
    styler.format(precision=0,
                formatter={('T-24hrs', 'success_rate'): "{:.0f} %",
                           ('T-7days', 'success_rate') : "{:.0f} %",
                            ('T-30days', 'success_rate'): "{:.0f} %",
                            ('T-24hrs', 'goodput'): "{:.0f} %",
                           ('T-7days', 'goodput') : "{:.0f} %",
                            ('T-30days', 'goodput'): "{:.0f} %"
                          },
                na_rep="-"
                 )
    styler.hide(axis='index')
    return styler

def column_rename(df, TagResult=False):
    if TagResult:
        df.columns = pd.MultiIndex.from_tuples([('site', 'match_exp_job_site'),
                                    ('T-24hrs', 'count_T_24hrs'), 
                                    ('T-24hrs', 'T_24hrs_success_rate'),
                                    ('T-7days', 'count_T_7days'), 
                                    ('T-7days', 'T_7days_success_rate'),
                                    ('T-30days', 'count_T_30days'),
                                    ('T-30days', 'T_30days_success_rate'),
                                           ])
        mapping = {'count_T_24hrs': 'count',
               'T_24hrs_success_rate': 'success_rate',
               'count_T_7days': 'count',
               'T_7days_success_rate': 'success_rate',
               'count_T_30days': 'count',
               'T_30days_success_rate': 'success_rate'
              }
    else:
        df.columns = pd.MultiIndex.from_tuples([('site', 'match_exp_job_site'),
                                        ('T-24hrs', 'count_T_24hrs'), 
                                        ('T-24hrs', 'T_24hrs_success_rate'), 
                                        ('T-24hrs', 'goodput_24hrs'),
                                        ('T-7days', 'count_T_7days'), 
                                        ('T-7days', 'T_7days_success_rate'),
                                        ('T-7days', 'goodput_week'),
                                        ('T-30days', 'count_T_30days'),
                                        ('T-30days', 'T_30days_success_rate'),
                                        ('T-30days', 'goodput_month')   
                                            ])
        mapping = {'count_T_24hrs': 'count',
                'T_24hrs_success_rate': 'success_rate',
                'count_T_7days': 'count',
                'T_7days_success_rate': 'success_rate',
                'count_T_30days': 'count',
                'T_30days_success_rate': 'success_rate',
                'goodput_24hrs': 'goodput',
                'goodput_week': 'goodput',
                'goodput_month': 'goodput'
                }
    df.rename(columns=mapping, level=1, inplace=True)
    return df

def create_html(list_of_objects):
    html_page = '''
        <html>
            <head>
            <title> 🔵 IGWN Job Statistics Summary</title>
            </head>
            <body>
                {}
            </body>
        </html>
    '''
    objects_html = '\n'.join(list_of_objects)
    email_html = html_page.format(objects_html)
    # write final html to a file
    with open('email.html', 'w') as f:
        f.write(email_html)

# adding gitlab ticket column 
def github_ticket_hyperlink(row):
    site_name = row['match_exp_job_site']
    return f'<a href="https://github.com/lscsoft/osg-igwn/issues?q=is%3Aissue+is%3Aopen+label%3Asite.{site_name}">{site_name}</a>'

def main():
    optmgr = OptionParser()
    opts = optmgr.parser.parse_args()
    host = opts.host
    port = opts.port
    index = opts.index
    days = opts.days
    username = opts.username
    password = opts.password
    hosts = host + ":" + str(port)

    attributes = ["CommittedTime","RemoteWallClockTime","ClusterId", "ProcId", "RecordTime", "ligosearchtag", "ExitCode", "LastJobStatus", "Status" , "Owner" , "ligosearchuser","job_site","match_exp_job_site","@timestamp"]
    datafile = "data.csv"

    #instanciating async elastic client request to elasticsearch  
    es = AsyncElasticsearch([hosts],basic_auth=(username, password))

    #creating query and other time filters used in the elastic query
    query , dask_time_filter , grafana_dashboard_link_cittest, grafana_dashboard_link_alljobs , time_filter =  timeassign(attributes=attributes, days=int(days))

    # creating asyncio loop and running it to load data from elastic endpoint and writing it to csv file
    loop = asyncio.get_event_loop()
    loop.run_until_complete(writer(esclient=es, hosts=hosts, query=query, index=index, username=username, password=password, datafile=datafile, attributes=attributes))

    # close the loop and connection to es
    es.close()

    # analyse the job data 
    result_df , result_all_df, tagresult_df = JobdataAnalyser(data_filename=datafile, dask_time_filter=dask_time_filter, attributes=attributes)

    # compare list of match_exp_job_site to all sites list from ticket https://github.com/lscsoft/osg-igwn/issues/436
    #reading site name from site_list.txt file
    site_name='site_list.txt'
    try:
        with open(site_name, 'r') as file:
            #converting the column containing site names to list 
            site_list = file.readlines()
            #strip newline characters
            site_list = [line.strip() for line in site_list]
    except:
        print("site list file missing")
    

    # extract list of sites which received any job
    job_sites = result_df['match_exp_job_site'].unique().tolist()
    # calculate missing sites
    missing_sites = [site for site in site_list if site not in job_sites]

    # including all the sites in efficiency table 
    missing_sites_df = pd.DataFrame({'match_exp_job_site': missing_sites})
    # appending to efficiency table the list of missing sites with 0 values in all the columns
    result_df = pd.concat([result_df, missing_sites_df], axis=0, ignore_index=True)
    
    #adding column to point github tickets
    result_df['match_exp_job_site'] =  result_df.apply(github_ticket_hyperlink, axis=1)

    #rearranging the columns to MultiIndex
    result_df = column_rename(result_df)
    
    jobresulthtml = (result_df
        .style
        .pipe(make_pretty)
        .to_html()
       )


    #rearranging the columns to MultiIndex
    tagresult_df = column_rename(tagresult_df, TagResult=True)

    tagresulthtml = (tagresult_df
        .style
        .pipe(make_pretty)
        .to_html()
       )
       

    # extract list of sites which received any job
    job_sites = result_all_df['match_exp_job_site'].unique().tolist()
    # calculate missing sites
    missing_sites_all = [site for site in site_list if site not in job_sites]

    # including all the sites in efficiency table 
    missing_sites_all_df = pd.DataFrame({'match_exp_job_site': missing_sites_all})
    # appending to efficiency table the list of missing sites with 0 values in all the columns
    result_all_df = pd.concat([result_all_df, missing_sites_all_df], axis=0, ignore_index=True)
    #rearranging the columns to MultiIndex
    result_all_df = column_rename(result_all_df)

    result_all_df = (result_all_df
        .style
        .pipe(make_pretty) 
        .to_html()
    )
    #sending email notification  
    #creating list of html objects which can be attached in the email notification
    html_objects = []

    ################################################ Job success rate html object starts here   ####################################################################

    #creating html object for job successrate and adding result dataframe    

    introduction = '<h3> IGWN pool payload job counts and success rates generated at {} </h3>\
    <p> IGWN pool analystics dashboard <br> \
    <li> <a href="{}">last 24 hours</a> </li>\
    <li> <a href="{}">last 7 days </a> </li> \
    <li> <a href="{}">last 30 days </a> </li> \
    <br> Note: please make sure that in grafana time setting its set to UTC </p>'.format(time_filter['end_time_human_readable'], grafana_dashboard_link_cittest['T_24hrs'], grafana_dashboard_link_cittest["T_7days"], grafana_dashboard_link_cittest["T_30days"])

    footer = '<ul> In the table columns are represented as \
        <li> count : count of total number of jobs from now  </li> \
        <li> success_rate : (number of jobs with ExitCode ==0 )/ Total number of Jobs for that site  </li> \
        <li> goodput : CommittedTime / RemoteWallClockTime </li> \
        <li> T-24hrs , T-7days, T-30days  respectively represent Time Period of Last 24hrs , 7days , and  30 days starting from UTC now </li> \
    </ul>'

    jobresulthtml = introduction + '<h4> Job count & success rate for Grid Exerciser tests (ligosearchtag=cit.scheduled.test) </h4>' + jobresulthtml + footer

    # appending to html_object list
    html_objects.append(jobresulthtml)

    ################################################ Job success rate html object ends here   ####################################################################

    ################################################ Job success rate for all tags html object starts here ##########################################################

    #creating html object for job successrate and adding result dataframe    

    introduction_alljobs = '<h4> Job count & success rate for all tags except for cit.scheduled.test, cit.oneshot.test </h4>\
    <a href="{}">last 24 hours</a> , <a href="{}">last 7 days </a> , <a href="{}">last 30 days </a> <br> Note: please make sure that in grafana time setting its set to UTC and disselect ligosearchtag variable for both of these tags (cit.scheduled.test,cit.oneshot.test) </p>'.format(grafana_dashboard_link_alljobs['T_24hrs'], grafana_dashboard_link_alljobs["T_7days"], grafana_dashboard_link_alljobs["T_30days"])

    result_all_df = introduction_alljobs + result_all_df


    # appending to html_object list
    html_objects.append(result_all_df)

    ################################################ Job success rate for all tags html object ends here ##########################################################



    ################################################ Tag success rate html object starts here ####################################################################

    tagresulthtml = '<h1>ligosearchtag job success table </h1>' + tagresulthtml
    #appending to html_object list
    html_objects.append(tagresulthtml)

    ################################################ Tag success rate html object ends here ####################################################################

    create_html(html_objects)
    
    
if __name__ == '__main__':
    main()
