#!/bin/bash

set -e 

#running main.py to generate report
python3 main.py --host="https://condor-elastic.nrp-nautilus.io" \
--port=443 \
--username=$ES_USERNAME \
--password=$ES_PASS \
--days=31 \
--index=$ES_INDEX


#setting ssh agent and public key authentication for scp to ldas-osg
eval $(ssh-agent -s)
echo "$SSH_PRADEEP_KEY" | tr -d '\r' | ssh-add -
mkdir -p ~/.ssh
chmod 700 ~/.ssh
scp -o "StrictHostKeyChecking no" email.html igwn-grid.test@ldas-osg.ligo.caltech.edu:~/public_html
printf "Subject: Job count and success rate report generated at - $(date)\nTo: $(cut -f1 subscribers.txt | tr '\n' ',') \nContent-Type: text/html\n\n" > email.txt; cat email.html >> email.txt
scp -o "StrictHostKeyChecking no" email.txt igwn-grid.test@ldas-osg.ligo.caltech.edu:~/
ssh -o "StrictHostKeyChecking no" igwn-grid.test@ldas-osg.ligo.caltech.edu 'sendmail -t < email.txt && rm email.txt'
