# Grid_Job_Analyzer


## Getting started

This script main.py reads data from elasticsearch endpoint, analyses them through dask dataframe and generates Job history reports for each site. To run the script successfully you need to keep the .gitlab-ci.yaml , main.py and main.sh files in the same directory of gitlab repository.

To send the report from your account , please change `--email_id`  and `--email_password` parameter in `main.sh` file. Similarly you will need elasticsearch client username and password to talk to elasticsearch endpoint and edit them in `main.sh`

```
--host="https://condor-elastic.nrp-nautilus.io" \    # elasticsearch hostname of endpoint
--port=443 \                            # port number of elasticsearch host to communicate and read data
--username="" \                         # username to authenticate elasticsearch client
--password="" \                         # password to authenticate elasticsearch client
--days=30 \                             # number of days for which you want to generate the htcondor job history reports
--index="htcondor-igwn*" \              # elasticsearch index name
--email_id="" \                         # sender email id to send reports
--email_password=""                     # email password for email sender

```
