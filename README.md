# grid-exerciser

Submits a DAG on a schedule to
`ldas-osg.ligo.caltech` with jobs to check grid functionality.

Parts of the HTCondor workflow trigger CI jobs to assess performance.

## TODO

* Tune time-out in `check_dag_status` to realistic walltime of DAG (currently 30 mins)
* Plot aggregated job metrics
* Aggregate, plot DAG metrics from dagman metrics file
* Expand test jobs: explicit IGWN conda environment test
* Expand test jobs: random authenticated CVMFS frame access

### Randomised frame access

Set up jobs:

1. `gwdatafind`: produce cache file with list of all frames in some time range, type
1. `random_read`: Run `head > /dev/null` on a random selection of 10 (say) frames in list 
1. Queue up 100 instances

## Workflow

The entire process is scheduled through the CI/CD workflow:

1. Generate files containing SSH keys and gitlab access tokens from CI
   environment variables.
1. Copy (scp gitlab -> schedd) grid exerciser workflow files (DAGMan, submit
   files, access token) to a working directory on the schedd.
1. Submit workflow (`ssh <schedd> condor_submit_dag`)
1. Each node in the DAG contains a POST script which dumps that job's history
   ClassAd to a file in the working directory and triggers the next CI job.
1. The triggered job pulls in the history classads (scp schedd -> gitlab) and
   checks for job failures.  If jobs have failed, the CI pipeline will fail and 
   we will get an email alert.  Summary information (`Cmd`, site, computation
   time, etc) from each job *process* is appended to a text file.
1. The summary information from each job is aggregated into a file for each job
   type and (will be) plotted and reported, perhaps analysed for performance
   degradation.


## Authentication

### Schedd

Jobs are submitted over ssh, with a private/public key pair generated offline
and uploaded to myligo.org.  The private key is saved as a variable in gitlab
and echoed into a key file at pipeline startup.

### Gitlab

Each DAG node runs a POST script which uploads logs to gitlab.  These use a
personal access token which, again, is echoed to file and sent out with the
jobs with permission 0600.

### X509

Jobs which require X509 authentication use a kerberos keytab held in *my*
`$HOME` on the schedd.  See
[computing.docs.ligo.org](https://computing.docs.ligo.org/guide/authentication/)
for details of generating the keytab. 

The kerberos ticket is generated with:

    kinit james.clark -t ./ligo.org.keytab

The X509 proxy is generated with: 

    ligo-proxy-init -k

## CI configuration

Some important notes about the CI config:

### Cancel autocancellation

* Normally, gitlab-ci wants to cancel jobs which have been superseded by
  subsequent instances of the same kind of job.  Our setup involves potential
  simultaneous operation of the same CI job to check the success of DAGMan
  nodes.  To *prevent* auto-cancellation of redundant pipelines: 

1. Go to Settings > CI/CD.
1. Expand General Pipelines.
1. Check the Auto-cancel redundant pipelines checkbox.
1. Click Save changes.

Use the interruptible keyword to indicate if a running job can be cancelled
before it completes

