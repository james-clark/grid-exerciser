# Authenticated CVMFS reading tests

These jobs attempt to:
1. Read the first 10MB in 3 frame files selected from `O3.txt` (either the first
   3 or a random selection).
1. Verify the frame files using the `framecpp_verify` application from the IGWN
   conda environment.  This reads each frame file in its entirety.


## *Important* notes

### `HAS_LIGO_FRAMES`

These tests do **NOT** (any longer) require the classad attribute
`HAS_LIGO_FRAMES==True`.  Instead, the jobs use `condor_htchirp` to inject a new
attribute into the classad `Chirp_HAD_LIGO_FRAMES` which records the value of
`HAS_LIGO_FRAMES` from the machine ad when the job ran.

This allows us to spray jobs across the entire IGWN pool and easily query condor
history for the rate of false positives (`HAS_LIGO_FRAMES` reported as True when
payload CVMFS access fails) and the rate of false negatives (`HAS_LIGO_FRAMES`
reported as False when payload CVMFS access succeeds).

To query for success/failure and compare with the results of the pilot test for
`HAS_LIGO_FRAMES`, you can just do e.g.:

    $ condor_history -limit 1000 <clusterid> -af Chirp_HAD_LIGO_FRAMES -af MATCH_EXP_JOB_GLIDEIN_Site -af ExitCode   | sort | uniq -c
             29 BNL false 0
             21 GATech false 0
             25 Michigan false 0
              6 NEMO false 0
             100 NIKHEF true 0
            173 Omaha false 0
              1 OSG_IN_ICTS_ALICE_HUMPTY false 0
             52 SU-ITS false 0
             11 Swan false 0
             13 UConn-HPC false 0
            543 Vanderbilt false 0
             26 Wisconsin false 0


Which, in this (contrived) example, shows the counts of jobs at each site, the
value of `HAS_LIGO_FRAMES` for that slot when the job ran and their exit codes (zero for success).

### X.509 proxy authentication

These sets of jobs use an X.509 user proxy for authentication to access
proprietory data.

Make sure this proxy is RFC 3820-compliant by generating it with the `-p` flag
in the invocation to `ligo-proxy-init`:

    $ ligo-proxy-init -p albert.einstein

Verify proxy compliance with e.g.:

    $ grid-proxy-info
    subject   : /DC=org/DC=cilogon/C=US/O=LIGO/CN=Albert Einstein albert.einstein@ligo.org/CN=366527452
    issuer    : /DC=org/DC=cilogon/C=US/O=LIGO/CN=Albert Einstein albert.einstein@ligo.org
    identity  : /DC=org/DC=cilogon/C=US/O=LIGO/CN=Albert Einstein albert.einstein@ligo.org
    type      : RFC compliant proxy
    strength  : 2048 bits
    path      : /tmp/x509up_u40348
    timeleft  : 276:59:56

i.e.:

    type      : RFC compliant proxy

(Note that `grid-proxy-info` is no longer installed at the system level but is
available in the IGWN conda environments.  If you do not have the environment
active, `voms-proxy-info` should return the same information.)
