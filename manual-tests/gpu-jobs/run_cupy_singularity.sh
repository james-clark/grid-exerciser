#!/bin/bash -e

echo "----------------------------------------------------------------"
echo "### PWD ###"
echo "hostname: $(hostname -f)"
export DOMAIN=$(hostname -d)
echo "ls -lha"
ls -lh

echo "### ENV ###"
echo "HOME=${HOME}"
echo "GW_SURROGATE=${GW_SURROGATE}"
echo "TMPDIR=${TMPDIR}"
echo "CUPY_CACHE_DIR=${CUPY_CACHE_DIR}"
echo "ls -a ${CUPY_CACHE_DIR}"

echo "### LD_LIBRARY_PATH ###"
echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
echo "ls /.singularity.d/libs"
ls -lRh /.singularity.d/libs

echo "### nvidia-smi ###"
/usr/bin/nvidia-smi || { echo "FAILED: nvidia-smi"; exit 1; }


echo "### cupy ###"
#/usr/bin/python ./run_cupy.py || { echo "FAILED: cupy"; exit 2; }
#/home/richard.oshaughnessy/RIFT_prodO3b/bin/python ./run_cupy.py || { echo "FAILED: cupy"; exit 2; }
#python ./run_cupy.py || { echo "FAILED: cupy"; exit 2; }
python ./run_cupy_singularity.py
#python /home/james.clark/Projects/osg-check/GPU-test/run_cupy.py || { echo "FAILED: cupy"; exit 2; }
echo "ls -lha ${CUPY_CACHE_DIR}"

