#!/usr/bin/env python
import os
import sys
import traceback
import time
start = time.time()
try:
    import cupy
except ImportError:
    print("*** cupy import failure ***")
    traceback.print_exc(file=sys.stderr)
    sys.exit(1)

print("Cupy config:")
try:
    cupy.show_config()
except:
    print("+++ cupy config failure +++")
    traceback.print_exc(file=sys.stderr)
    sys.exit(2)

try:
    start = time.time()
    #cupy_array = cupy.ones((500,500,500))
    cupy_array = cupy.ones(1000)
    cupy.cuda.Stream.null.synchronize()
    end = time.time()
    print(f"cupy_array creation took {end-start}s")
except:
    print("+++ cupy array failure +++")
    traceback.print_exc(file=sys.stderr)
    sys.exit(3)

try:
    start = time.time()
    end = start
    while end-start < 300:
        cupy_array *= cupy_array
        cupy.cuda.Stream.null.synchronize()
        end = time.time()
    print(f"cupy_array square took {end-start} s")
except:
    print("+++ cupy square failure +++")
    traceback.print_exc(file=sys.stderr)
    sys.exit(4)

try:
    domain = os.environ['DOMAIN']
except:
    print("DOMAIN var not set")
    domain = "unknown"

print(f"DIAGNOSTIC: host {domain} {time.time() - start:.2f} s")
print(f"=== CUPY SUCCESS ===")
sys.exit(0)
