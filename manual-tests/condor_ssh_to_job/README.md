# `condor_ssh_to_job` test

Launches a set of vanilla Universe `/bin/sleep` pilot paylod jobs and a matching
set of local Universe jobs which will attempt to `condor_ssh_to_job` the
`/bin/sleep` jobs.

Dev notes:

1. Launch the sleep jobs (may want some number crunching jobs so we don't alarm
   admins).  Let them run *forever*.
1. Launch a `condor_ssh_to_job` job for each of these and poll until
   sleep-partner enters the run-state.
1. When the sleep-partner starts running: 
 - Record the `GLIDEIN_Site` of the sleep partner via condor chirp as e.g.
   `CONDOR_SSH_TARGET=$(condor_q <sleep-job-id> -af MATCH_EXP_JOB_GLIDEIN_Site)`
 - Attempt a `condor_ssh_to_job`.  


Do I even need the sleep jobs?

E.g., I could periodically get the list of all running jobs from the
grid-exerciser dag and use those (maybe just limit it to the numpy/cupy jobs).

Easy: 1 `condor_ssh` job for all running jobs
Tricky: N `condor_ssh` jobs for N runnng jobs

Use case for DAGMan service thing?

i.e., what if i had 10 (say) SERVICE nodes
(https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html#service-node)

which watched for specific numpy/cupy nodes and if they see them run, try to
ssh-to-job?

This features seems useful: 

> If a DAGMan workflow finishes while there are SERVICE nodes still running, it will shut these down and then exit the workflow successfully.

With a bunch of different nodes like:

    numpy-oasis+numpy-oasis-97
    numpy-oasis+numpy-oasis-98
    numpy-oasis+numpy-oasis-99

We should put the SERVICE nodes in their own splice like:

    SERVICE condor-ssh-to-job-1 condor-ssh-to-job.sub
    VARS condor-ssh-to-job-1 nodename="numpy-oasis+numpy-oasis-1"

    SERVICE condor-ssh-to-job-2 condor-ssh-to-job.sub
    VARS condor-ssh-to-job-2 nodename="numpy-oasis+numpy-oasis-2"
    ...
    SERVICE condor-ssh-to-job-N condor-ssh-to-job.sub
    VARS condor-ssh-to-job-N nodename="numpy-oasis+numpy-oasis-N"

Assuming SERVICE nodes take VARS...
