# Manual test jobs

This directory contains collections of submit files and scripts for standard tests of IGWN grid functionality.

Jobs run under accounting tag `cit.oneshot.test` (as opposed to the `cit.scheduled.test` used for automatically scheduled periodic jobs).

## Basic CPU / GPU availability

- `cpu-jobs/IGWN-CPUs-oasis.sub`: Creates and squares `numpy` arrays for five minutes duration.  Numpy obtained from IGWN conda environments in oasis.
- `cpu-jobs/IGWN-CPUs-singularity.sub`: Creates and squares `numpy` arrays for five minutes duration.  Numpy obtained from a popular (RIFT) singularity image.
- `gpu-jobs/IGWN-GPUs-singularity.sub`: Creates and squares `cumpy` arrays for five minutes duration.  Cupy obtained from a popular (RIFT) singularity image.

Note:
- The Scripts `test_cpus_at_site` can be used to generate submit files to target a specific `GLIDEIN_Site`, using the `.j2` Jinja2 templates (**requires** `jinja2`.).  Produces a sub-directory with the same submit files as above with `+DESIRED_Sites=<site>`.
- Submit files use relative paths, so jobs should be submitted from the same directory as the submit file.
- GPU jobs still also request 1 CPU.
- :construction: `cupy` has recently been/will be available in oasis, so expect GPU oasis jobs to arrive soon.

## Authenticated CVMFS availability

- `auth-cvmfs-data/read-frames.sub`: Reads the first 100M (`/bin/head`) from the first 10 frame files listed in `O3.txt`.
- `auth-cvmfs-data/read-frames-gpu.sub`: Reads the first 100M (`/bin/head`) from the first 10 frame files listed in `O3.txt` on GPU nodes.
- `auth-cvmfs-data/read-random-frames.sub`: Reads the first 100M (`/bin/head`) from a random selection of 10 frame files listed in `O3.txt`.
- `auth-cvmfs-data/read-random-frames-gpu.sub`: Reads the first 100M (`/bin/head`) from a random selection of 10 frame files listed in `O3.txt` on GPU nodes.

Note:
- The script `test_auth_cvmfs_at_sites` can be used to generate submit files to target a specific `GLIDEIN_Site`, using the `.j2` Jinja2 templates (**requires** `jinja2`.).  Produces a sub-directory with the same submit files as above with `+DESIRED_Sites=<site>`.
- We have separate jobs for GPUs and CPUs because we can only access the GPU slots if we request a GPU.
- The GPU jobs still also request 1 CPU.
- We have separate jobs for a fixed list of files and a random list: this is helpful for distinguishing cases where there is a problem with caching new data and accessing previously cached data (really a site-specific test).


:construction: **Coming soon** (just needs some clean up and templates): 
- similar site-specific setup scripts to the CPU/GPU test jobs in the previous section.
- `stashcp` jobs which provide useful debug information on the nearest/available caches at a given site.
