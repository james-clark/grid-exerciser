#!/bin/bash -e

echo "----------------------------------------------------------------"
echo "### PWD ###"
echo "hostname: $(hostname -f)"
export DOMAIN=$(hostname -d)
echo "ls -lha"
ls -lh

echo "### ENV ###"
echo "HOME=${HOME}"
echo "GW_SURROGATE=${GW_SURROGATE}"
echo "TMPDIR=${TMPDIR}"
echo "ls -a ${CUPY_CACHE_DIR}"
env

echo "### LD_LIBRARY_PATH ###"
echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
echo "ls /.singularity.d/libs"
ls -lRh /.singularity.d/libs


echo "### numpy ###"
python ./run_numpy_singularity.py || { echo "FAILED: cupy"; exit 2; }
echo "ls -lha ${CUPY_CACHE_DIR}"

