#!/bin/bash -e

echo "----------------------------------------------------------------"
echo "### PWD ###"
echo "hostname: $(hostname -f)"
export DOMAIN=$(hostname -d)
echo "ls -lha"
ls -lh

echo "### ENV ###"
env

echo "### numpy ###"
/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py38/bin/python ./run_numpy_oasis.py || { echo "FAILED: numpy"; exit 2; }
