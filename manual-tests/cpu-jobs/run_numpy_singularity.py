#!/usr/bin/env python
import os
import sys
import traceback
import time
start = time.time()
try:
    import numpy
except ImportError:
    print("*** numpy import failure ***")
    traceback.print_exc(file=sys.stderr)
    sys.exit(1)

print("numpy config:")
try:
    numpy.show_config()
except:
    print("+++ numpy config failure +++")
    traceback.print_exc(file=sys.stderr)
    sys.exit(2)

try:
    start = time.time()
    numpy_array = numpy.ones(1000)
    end = time.time()
    print(f"numpy_array creation took {end-start}s")
except:
    print("+++ numpy array failure +++")
    traceback.print_exc(file=sys.stderr)
    sys.exit(3)

try:
    start = time.time()
    end = start
    while end-start < 600:
        numpy_array *= numpy_array
        end = time.time()
    print(f"numpy_array square took {end-start} s")
except:
    print("+++ numpy square failure +++")
    traceback.print_exc(file=sys.stderr)
    sys.exit(4)

try:
    domain = os.environ['DOMAIN']
except:
    print("DOMAIN var not set")
    domain = "unknown"

print(f"DIAGNOSTIC: host {domain} {time.time() - start:.2f} s")
print(f"=== numpy SUCCESS ===")
sys.exit(0)
