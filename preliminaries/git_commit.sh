#!/bin/bash
echo "whoami:"
whoami

/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/git clone https://condor:xGipgLr_z8-cAZxgZPBF@git.ligo.org/james-clark/igwn-osg-mon.git

cd igwn-osg-mon

git config --global push.default simple

hostname -f > commit.txt
date >> commit.txt

git add commit.txt
git commit -m "[condor] $(hostname -f): $(date)"
git push

exit 0
