#!/bin/bash
WORKDIR=/home/james.clark/igwn-osg-mon
SCHEDD=ldas-osg.ligo.caltech.edu

SUBMITSRC=https://git.ligo.org/james-clark/igwn-osg-mon/-/raw/master/readframe_nogrid.sub
SUBMITFILE=submit.sub

ssh ${SCHEDD} << EOF
cd ${WORKDIR}
curl -o ${SUBMITFILE} ${SUBMITSRC}
condor_submit ${SUBMITFILE}
rm submit.sub
EOF
