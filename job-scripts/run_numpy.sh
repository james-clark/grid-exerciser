#!/bin/bash -e
echo "hostname: $(hostname -f)"
echo "pwd: $(pwd)"
echo "ls -laRh:"
ls -laRh
env

echo "### numpy ###"
python3 ./run_numpy.py || { echo "FAILED: numpy"; exit 2; }
