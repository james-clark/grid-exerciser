#!/bin/bash

# CPUs

osg_sites=$(condor_status -any -pool gfactory-2.opensciencegrid.org \
    -const 'MyType=?="glidefactory" &&
    (stringListMember("LIGO",GLIDEIN_Supported_VOs) ||
    stringListMember("VIRGO",GLIDEIN_Supported_VOs))'  \
    -af GLIDEIN_Site | sort | uniq)

cern_sites=$(condor_status -any -pool vocms0207.cern.ch  \
    -const 'MyType=?="glidefactory" &&
    (stringListMember("LIGO",GLIDEIN_Supported_VOs) ||
    stringListMember("VIRGO",GLIDEIN_Supported_VOs))' \
    -af GLIDEIN_Site | sort | uniq)

all_sites=$(echo -e "$osg_sites\n$cern_sites" | awk '!seen[$0]++')

echo "### All CPU sites:\n"
echo ${all_sites} | tr ' ' '\n'
echo ${all_sites} | tr ' ' '\n' > cpu-sites-list.txt

# GPUs

osg_sites=$(condor_status -any -pool gfactory-2.opensciencegrid.org \
    -const 'MyType=?="glidefactory" &&
    (stringListMember("LIGOGPU",GLIDEIN_Supported_VOs) ||
    stringListMember("VIRGOGPU",GLIDEIN_Supported_VOs))'  \
    -af GLIDEIN_Site | sort | uniq)

cern_sites=$(condor_status -any -pool vocms0207.cern.ch  \
    -const 'MyType=?="glidefactory" &&
    (stringListMember("LIGOGPU",GLIDEIN_Supported_VOs) ||
    stringListMember("VIRGOGPU",GLIDEIN_Supported_VOs))'  \
    -af GLIDEIN_Site | sort | uniq)

all_sites=$(echo -e "$osg_sites\n$cern_sites" | awk '!seen[$0]++')

echo "### All GPU sites:\n"
echo ${all_sites} | tr ' ' '\n'
echo ${all_sites} | tr ' ' '\n' > gpu-sites-list.txt
