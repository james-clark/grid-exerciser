#!/bin/bash -e
echo "hostname: $(hostname -f)"
echo "pwd: $(pwd)"
echo "ls -laRh:"
ls -laRh
env

echo "### numpy ###"
/cvmfs/software.igwn.org/conda/envs/igwn/bin/python ./run_numpy_conda.py || { echo "FAILED: numpy"; exit 2; }
